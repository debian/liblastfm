liblastfm (1.1.0-5) unstable; urgency=medium

  * QA upload.
  * debian/rules:
    + Enable full hardening.
    + Explicitly enable LTO. (Closes: #1015499)
  * debian/*.symbols: Refresh symbols with LTO enabled.

 -- Boyuan Yang <byang@debian.org>  Sun, 06 Nov 2022 13:02:23 -0500

liblastfm (1.1.0-4) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * debian/copyright: use spaces rather than tabs to start continuation lines.
  * Set upstream metadata fields: Repository.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 04 Jun 2022 23:05:07 +0100

liblastfm (1.1.0-3) unstable; urgency=medium

  * QA upload.
  * debian/rules: Pass -DCMAKE_BUILD_RPATH_USE_ORIGIN=ON to
    dh_auto_configure.
  * debian/control: Set Rules-Requires-Root to "no".
  * debian/control: Update Standards-Version to 4.6.1.

 -- Vagrant Cascadian <vagrant@reproducible-builds.org>  Wed, 11 May 2022 17:19:46 -0700

liblastfm (1.1.0-2) unstable; urgency=medium

  * QA upload.
  * Reconsolidate symbols using buildd logs and pkgkde-symbolshelper.

 -- Boyuan Yang <byang@debian.org>  Sun, 03 Apr 2022 10:30:32 -0400

liblastfm (1.1.0-1) unstable; urgency=medium

  * QA upload.
  * New upstream version 1.1.0
  * Orphan package.
  * debian/: Apply wrap-and-sort -abst.
  * debian/control:
    + Bump debhelper compat to v13.
    + Bump Standards-Version to 4.6.0.
    + Use correct Vcs-* URLs.
    + Drop manual -dbg packages.
    + Use github as homepage.
  * debian/patches: Drop useless patches.
  * debian/liblastfm-fingerprint5-1.symbols: Prepare for symbol refresh.

 -- Boyuan Yang <byang@debian.org>  Wed, 30 Mar 2022 16:43:45 -0400

liblastfm (1.0.9-1.2) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS with GCC-11 by removing dynamic exception specifications,
    patch taken from upstream PR. (Closes: #984114)

 -- Florian Ernst <florian@debian.org>  Sun, 13 Feb 2022 08:04:58 +0100

liblastfm (1.0.9-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Drop Qt4 build, no reverse dependencies left in the archive and Qt4
    is going away (Closes: #875025)

 -- Moritz Muehlenhoff <jmm@debian.org>  Fri, 23 Aug 2019 23:37:23 +0200

liblastfm (1.0.9-1) unstable; urgency=low

  * New upstream release. (Closes: #805081)
  * Set myself as the new maintainer with permission of John Stamp the original
    maintainer.
  * debian/rules;debian/control;debian/*5*:
    - Dual build: Qt4 & Qt5
  * debian/control:
    - Standards-Version: 3.9.6
  * update debian/copyright
  * Remove patch applied upstream:
    - fix_q_os_x11.patch
  * Add patch:
    - change_target-name.patch

 -- Stefan Ahlers <stef.ahlers@t-online.de>  Fri, 20 Nov 2015 11:40:00 +0100

liblastfm (1.0.8-3) unstable; urgency=medium

  * Change my email address in debian/control, debian/copyright.
  * Mark some symbols as optional to fix build with gcc 5.

 -- John Stamp <jstamp@mehercule.net>  Mon, 29 Jun 2015 15:30:29 -0700

liblastfm (1.0.8-2) unstable; urgency=low

  * Check Q_OS_UNIX definition instead of nonexistent Q_OS_X11.  This fixes
    the cache and config paths that broke in the last upload.

 -- John Stamp <jstamp@users.sourceforge.net>  Wed, 04 Sep 2013 11:06:05 -0700

liblastfm (1.0.8-1) unstable; urgency=low

  * New upstream release.
  * Update debian/watch file.  Thanks to Bart Martens.
  * Mark the -dbg and -dev packages Multi-Arch: same
  * Update symbols for liblastfm1

 -- John Stamp <jstamp@users.sourceforge.net>  Tue, 03 Sep 2013 18:27:52 -0700

liblastfm (1.0.7-2) unstable; urgency=low

  * Bump Standards-Version to 3.9.4.  No changes needed.
  * Upload to unstable.

 -- John Stamp <jstamp@users.sourceforge.net>  Sat, 01 Jun 2013 07:09:27 -0700

liblastfm (1.0.7-1) experimental; urgency=low

  * New upstream release.
  * Update symbols files.

 -- John Stamp <jstamp@users.sourceforge.net>  Wed, 27 Feb 2013 08:24:42 -0800

liblastfm (1.0.3-1) experimental; urgency=low

  * New upstream release.
  * Remove patches applied upstream:
    - dont_leak_qnetwork_replies_in_audioscrobbler.patch
    - fix_no_return_in_nonvoid.patch
    - fix_wrong_parameter_name_in_removetag.patch
  * Update symbols files

 -- John Stamp <jstamp@users.sourceforge.net>  Wed, 17 Oct 2012 10:38:45 -0700

liblastfm (1.0.2-1) experimental; urgency=low

  * New upstream release.
  * Update symbols files.
  * Add patches:
    - fix_wrong_parameter_name_in_removetag.patch
    - dont_leak_qnetwork_replies_in_audioscrobbler.patch
    - fix_no_return_in_nonvoid.patch
  * Update debian/copyright and debian/watch; upstream changed the location of
    its release repository.

 -- John Stamp <jstamp@users.sourceforge.net>  Wed, 12 Sep 2012 17:27:02 -0700

liblastfm (1.0.1-1) experimental; urgency=low

  * New upstream
  * Remove fix-build-with-ruby1.9.patch
  * Rename lib packages due to SONAME change
  * Modify Build-Depends since upstream now uses cmake
  * Use debhelper 9 for multiarch and hardened build flags
  * Fix lintian warnings description-synopsis-starts-with-article,
    binary-control-field-duplicates-source
  * Add libqt4-dev and misc:Depends to liblastfm-dev; remove shlibs:Depends
    from liblastfm-dbg
  * Add a watch file
  * Bump Standards-Version to 3.9.3
  * Update debian/copyright
  * Add symbols files and use pkgkde_symbolshelper in dh invocation
  * Add --parallel to dh invocation

 -- John Stamp <jstamp@users.sourceforge.net>  Thu, 26 Jul 2012 21:23:10 -0700

liblastfm (0.4.0~git20090710-2) unstable; urgency=medium

  * Fix compilation with ruby1.9.1 >= 1.9.2 (Closes: #676104)
  * Use source format 3.0 (quilt)

 -- John Stamp <jstamp@users.sourceforge.net>  Mon, 16 Jul 2012 20:21:22 -0700

liblastfm (0.4.0~git20090710-1) unstable; urgency=low

  * New upstream release from git snapshot.
  * Bump Standards-Version to 3.8.2 (no changes needed).
  * Shorten package descriptions.
  * Bump shlibs version for liblastfm0.
  * Rename debug package.

 -- John Stamp <jstamp@users.sourceforge.net>  Tue, 14 Jul 2009 11:14:08 -0700

liblastfm (0.3.0-1) unstable; urgency=low

  * Initial release (Closes: #524948)

 -- John Stamp <jstamp@users.sourceforge.net>  Thu, 04 Jun 2009 21:21:45 -0700
