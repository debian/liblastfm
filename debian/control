Source: liblastfm
Section: libs
Priority: optional
Maintainer: Debian QA Group <packages@qa.debian.org>
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 libfftw3-dev,
 libsamplerate0-dev,
 pkg-kde-tools,
 qtbase5-dev,
Standards-Version: 4.6.1
Homepage: https://github.com/lastfm/liblastfm/
Vcs-Git: https://salsa.debian.org/debian/liblastfm.git
Vcs-Browser: https://salsa.debian.org/debian/liblastfm
Rules-Requires-Root: no

Package: liblastfm-fingerprint5-1
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Last.fm fingerprinting library (Qt5 build)
 liblastfm is a collection of C++/Qt5 libraries provided by Last.fm for use
 with their web services.
 .
 This library lets you fingerprint decoded audio tracks and fetch metadata
 suggestions for them.

Package: liblastfm5-1
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Last.fm web services library (Qt5 build)
 liblastfm is a collection of C++/Qt5 libraries provided by Last.fm for use
 with their web services.
 .
 This package contains the base web services library.

Package: liblastfm5-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 liblastfm-fingerprint5-1 (= ${binary:Version}),
 liblastfm5-1 (= ${binary:Version}),
 ${misc:Depends},
Description: Last.fm web services library (Qt5 build) - development files
 liblastfm is a collection of C++/Qt5 libraries provided by Last.fm for use
 with their web services.
 .
 This package contains the development files. Developers working on a new
 client will need to request an API key. See README for more details.
